import React, {useContext} from 'react';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

import { GlobalContext } from '../context/GlobalState';

export const Transaction = ({ transaction }) => {
  const { deleteTransaction } = useContext(GlobalContext);

  const sign = transaction.amount < 0 ? '-' : '+';

  const promptConfirmation = function({ text, id}) {
    confirmAlert({
      title: 'Confirm to delete transaction',
      message: `Are you sure you want to delete your transaction "${text}"?`,
      buttons: [
        {
          label: 'Yes',
          onClick: () => deleteTransaction(id)
        },
        {
          label: 'No',
          onClick: () => console.log("Transaction won't be deleted")
        }
      ]
    });
  }

  return (
    <li className={transaction.amount < 0 ? 'minus' : 'plus'}>
      {transaction.text} 
      <span>{sign}${Math.abs(transaction.amount)}</span>
      <div className="actions">
        <button className="actions__btn delete" onClick={() => promptConfirmation({ text: transaction.text, id: transaction.id})}>
            <img alt="Delete transaction" src="delete.png"/>
        </button>
        <button className="actions__btn edit" onClick={() => console.log(transaction)}>
            <img alt="Edit transaction" src="edit.png"/>
        </button>
      </div>
    </li>
  )
}
